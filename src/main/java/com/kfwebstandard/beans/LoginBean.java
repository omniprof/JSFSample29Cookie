package com.kfwebstandard.beans;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * A Context Dependency Injection managed bean
 *
 * @author Ken
 * @version 1.0
 *
 */
@Named("loginBean")
@RequestScoped
public class LoginBean implements Serializable {

    private String name;
    private String password;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
